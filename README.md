# fcm-listener-node

the name is a lie it just registers the device _for now_

MRs are welcome

huge thanks to:

- microG Project's [GmsCore implementation](https://github.com/microg/android_packages_apps_GmsCore)
- MattieuLemoine's [push-receiver](https://github.com/MatthieuLemoine/push-receiver/)

use in your project: https://yarnpkg.com/package/fcm-listener

chat on matrix: [#erupcja-fcm-listener-node:laura.pm](https://matrix.to/#/#erupcja-fcm-listener-node:laura.pm) / [#erupcja:laura.pm](https://matrix.to/#/#erupcja:laura.pm)
