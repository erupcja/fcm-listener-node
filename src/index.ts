import checkin from './requests/checkin';
import c2dm from './requests/c2dm';
import { generateFid } from './utils';
import { GoogleData } from './types';

export const register = async (data: GoogleData) => {
  const fid = generateFid();
  const aid = await checkin({
    // first checkin, no checkinData for now
    androidId: '0',
    securityToken: '',
    // initial digest from microG
    digest: '1-929a0dca0eee55513280171a8585da7dcd3700f8',
  });
  const firebaseToken = await c2dm(data, aid, fid);
  return {
    firebaseToken,
    aid,
    fid,
  };
};

export { GoogleData, CheckinData } from './types';
