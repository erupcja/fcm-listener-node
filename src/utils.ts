export const generateString = (len: number, charLimit?: number | null, chars?: string | null) =>
  'd'
    .repeat(len)
    .split('')
    .map(
      () =>
        (chars || '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_')[
          Math.floor(Math.random() * (charLimit || (chars ? chars.length : 64)))
        ],
    )
    .join('');

export const generateFid = () => generateString(22);

export const generateInfo = () => generateString(22, 62);
