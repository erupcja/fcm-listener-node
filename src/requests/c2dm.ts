import got from 'got';
import qs from 'querystring';
import { generateInfo } from '../utils';
import { GoogleData, CheckinData } from '../types';

export default async (
  { googleAppId, gcmSenderId, packageName, versionName, versionCode, fingerprint }: GoogleData,
  { androidId, securityToken }: CheckinData,
  fid: string,
) =>
  got
    .post('https://android.clients.google.com/c2dm/register3', {
      body: qs.stringify({
        'X-subtype': gcmSenderId,
        sender: gcmSenderId,
        'X-app_ver': versionCode,
        'X-osv': '23',
        'X-cliv': 'fiid-20.1.1',
        'X-gmsv': '16089023',
        'X-appid': fid,
        'X-scope': '*',
        'X-gmp_app_id': googleAppId,
        'X-firebase-app-name-hash': 'R1dAH9Ui7M-ynoznwBdw01tLxhI', // sha1 base64url of "[DEFAULT]"
        'X-app_ver_name': versionName,
        app: packageName,
        device: androidId,
        app_ver: versionCode,
        info: generateInfo(),
        gcm_ver: '16089023',
        plat: '0',
        cert: fingerprint['SHA-1'].toLowerCase(),
        target_ver: '28',
      }),
      headers: {
        Authorization: `AidLogin ${androidId}:${securityToken}`,
        app: packageName,
        gcm_ver: '16089023',
        'User-Agent': 'Android-GCM/1.5 (generic_x86_64 MASTER)',
        'content-type': 'application/x-www-form-urlencoded',
      },
    })
    .then((res) => qs.parse(res.body))
    .then((res) => res.token as string);
