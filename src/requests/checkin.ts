import got from 'got';
import protobuf from 'protobufjs';
import path from 'path';
import { generateString } from '../utils';
import { CheckinData } from '../types';

const checkinProto = protobuf.loadSync(path.resolve(__dirname, '..', 'protos', 'checkin.proto'));
const checkinRequest = checkinProto.lookupType('CheckinRequest');
const checkinResponse = checkinProto.lookupType('CheckinResponse');

export default async ({ androidId, securityToken, digest }: CheckinData) => {
  const first = androidId === '0';
  return (
    got
      .post('https://android.googleapis.com/checkin', {
        body: Buffer.from(
          checkinRequest
            .encode(
              checkinRequest.create({
                androidId,
                digest,
                checkin: {
                  build: {
                    fingerprint: `xiaomi/razor/kenzo:6.0.1/${generateString(
                      6,
                      62,
                    ).toUpperCase()}/16089023:user/release-keys`,
                    hardware: 'kenzo',
                    brand: 'xiaomi',
                    bootloader: 'unknown',
                    clientId: 'android-google',
                    time: 1556821484,
                    packageVersionCode: 16089023,
                    device: 'kenzo',
                    sdkVersion: 23,
                    model: 'Redmi Note 3',
                    manufacturer: 'xiaomi',
                    product: 'redmi',
                    otaInstalled: false,
                  },
                  lastCheckinMs: 0,
                  event: [
                    {
                      tag: first ? 'event_log_start' : 'system_update',
                      value: first ? null : '1536,0,-1,NULL',
                      timeMs: new Date().getTime(),
                    },
                  ],
                  roaming: 'MOBILE:LTE:',
                  userNumber: 0,
                },
                loggingId: generateString(20, 10),
                securityToken: first ? null : securityToken,
                version: 3,
                otaCert: ['--no-output--'],
                serial: generateString(8, 16),
                deviceConfiguration: {
                  touchScreen: 3,
                  keyboardType: 1,
                  navigation: 1,
                  screenLayout: 2,
                  hasHardKeyboard: false,
                  hasFiveWayNavigation: true,
                  densityDpi: 400,
                  glEsVersion: 196608,
                  sharedLibrary: [
                    'com.google.android.gms',
                    'android.hardware.touchscreen',
                    'android.software.webview',
                  ],
                  nativePlatform: ['arm64-v8a'],
                  widthPixels: 1080,
                  heightPixels: 1920,
                  locale: ['en-US', 'en-UK', 'pl'],
                  glExtension: [],
                },
                fragment: first ? 0 : 1,
              }),
            )
            .finish(),
        ),
        headers: {
          'Content-type': 'application/x-protobuffer',
          // 'Content-encoding': 'gzip',
          // 'Accept-encoding': 'gzip',
          'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; kenzo)',
        },
      })
      .then((res) => res.rawBody)
      .then((res) => checkinResponse.decode(res).toJSON())
      // getting rid of that shitload
      .then(
        ({ androidId, securityToken, digest }) =>
          ({ androidId, securityToken, digest } as {
            androidId: string;
            securityToken: string;
            digest?: string;
          }),
      )
      // digest is the originally provided one if undefined in response
      .then((res) => ({ ...res, digest: res.digest || digest }))
  );
};
