export interface GoogleData {
  crashlyticsBuildId?: string;
  defaultWebClient?: string;
  firebaseDatabaseUrl?: string;
  gcmSenderId: string;
  googleApiKey: string;
  googleAppId: string;
  crashlyticsApiKey?: string;
  googleStorageBucket?: string;
  projectId?: string;
  packageName: string;
  versionCode: string;
  versionName: string;
  fingerprint: {
    'SHA-256'?: string;
    'SHA-1': string;
    MD5?: string;
  };
}

export interface CheckinData {
  androidId: string;
  securityToken: string;
  digest: string | null;
}
